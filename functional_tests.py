#!/usr/bin/env python3

import backupsh
import os
import subprocess
import unittest

#Functional Test-The Story:
# HorrificBotnet wants to back up their data.
# So they download this helpful application and install it :)

# We should probably make sure the help menu displays properly
class TestHelpMenu(unittest.TestCase):
    def test_if_help_displays(self):
        """Help should be displayed"""
        try:
            backupsh.display_help()
        except AttributeError:
            self.fail("No display_help() function found!")

# After some study, User figures out how to create a backup profile
# This includes various options, contexts, commands and system packages
# User -> Main Backup -> rsync
class TestLocalBackup(unittest.TestCase):
    def setUp(self):
        self.tmp_dir="/tmp/test-dir/"
        self.tmp_file=os.path.join(self.tmp_dir,'test.txt')
        self.backedup_file="/tmp/backup-dir/"
        os.makedirs(self.tmp_dir)
        f = open(self.tmp_file, 'w+').close()
        
    def tearDown(self):
        os.remove(self.tmp_file)
        os.removedirs(self.tmp_dir)
    
    def test_if_backup_succeeds(self):
            self.assertTrue(os.path.exists(self.backedup_file))
# User -> Network Drive -> rsync through ssh
# User -> System packages (dpkg)

# User notices backups are being performed in the background but wants to
# update their phone while their system is being backed up.
# 
# Make sure that they can do that if there are no conflicts, 
# but stop them if we find out that phone is already being backed up.

# HorrificBotnet is so pleased with the app they want to run it on a schedule!
# Let's make sure we can install it with systemd

# The most important part of back ups are being able to restore from them!

if __name__ == "__main__":
    unittest.main()
