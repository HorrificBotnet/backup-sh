# readme
Created domingo 13 enero 2019

Goal
----
Back up all devices based on user defined routines by simply being connected to the computer.

### Data ↔ Code separation
Create a profile system that I can use to explain the process of backing up at a high level and leave the execution up to the implementation.

* i.e: Write a python script that can read a settings file which will understand how to execute the back up, but could also be understood by a bash or other script


#### Hierarchy of abstraction

Config contains all user data
Profiles are settings per user, devices/storage unit, contexts, etc
Routines are actual commands to run for each device based on the criteria met
Commands are any work a user wants done to files. Runs through the shell.

Logic Flow: ID User -> ID Drives Available -> Perform relevant backup routines

#### Settings file example
``[pootz]``
``wiisd="/media/pootz/wiisd"``
``phone="oneplus:/storage/emulated/0"``
``elements="/media/pootz/Elements"``
``-wiisd-``
``echo "Backing up Gamecube games from {wiisd}"``
``rsync --options source dest``
``echo "You get the idea..."``
``-phone-``
``# more code, etc``
``-main-``
``rsync source dest``
``dpkg``
``[media]``
``# more code...``



